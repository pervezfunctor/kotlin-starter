package seartipy.examples

import kotlin.test.*

class CombinatoricsTest {
    @Test
    fun factorialTest() {
        assertEquals(1, factorial(0))
        assertEquals(1, factorial(1))
        assertEquals(120, factorial(5))
    }
}