package seartipy.examples

fun factorial(n: Int): Int {
    return (1..n).fold(1) { x, y -> x * y }
}
